package domains

import play.api.libs.json.Json

case class Notification (table: String, action: String, data: TokenMedia)
object Notification {
  implicit val fmt = Json.format[Notification]
}
