package domains

import play.api.libs.json.Json

case class TokenMedia(identity_token: String, media: String, deleted: Boolean)

object TokenMedia {
  implicit val fmt = Json.format[TokenMedia]
}