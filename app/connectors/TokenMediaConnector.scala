package connectors

import domains.TokenMedia
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import utils.AccessTokenGenerator
import javax.inject._


@Singleton
class TokenMediaConnector @Inject()(ws: WSClient) {
  val tokenSecret = "super-secret-value-noone-knows"

  val adminToken = new AccessTokenGenerator(tokenSecret, 6000).newAccessToken("admin", Some("admin")).token

  def create(tokenMedia: TokenMedia) = {
    ws.url("http://dockerhost:9060/usermedia/admin/token-media")
      .withHeaders(("Authorization", s"Bearer $adminToken"))
      .post(Json.obj("token" -> tokenMedia.identity_token, "redemptionCode" -> tokenMedia.media))
  }

  def delete(tokenMedia: TokenMedia) = {
    ws.url("http://dockerhost:9060/usermedia/admin/token-media")
      .withHeaders(("Authorization", s"Bearer $adminToken"))
      .withMethod("DELETE")
      .withBody(Json.obj("token" -> tokenMedia.identity_token, "redemptionCode" -> tokenMedia.media)).execute()
  }
}