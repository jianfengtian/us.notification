import java.sql.Connection

import actors.SchedulerActor
import com.google.inject.AbstractModule
import connectors.TokenMediaConnector
import jobs.Job
import org.apache.commons.dbcp.PoolingDataSource
import play.api.libs.concurrent.AkkaGuiceSupport
import scalikejdbc.{ConnectionPool, DB}

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.

 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */


  class Module extends AbstractModule with AkkaGuiceSupport {

  override def configure() = {
    Class.forName("org.postgresql.Driver")
    ConnectionPool.singleton("jdbc:postgresql://dockerhost:5632/test", "picsolve", "ahn7kooPaibai3de")
    ConnectionPool.dataSource().asInstanceOf[PoolingDataSource].setAccessToUnderlyingConnectionAllowed(true)

    val connection = ConnectionPool.borrow()

    bind(classOf[Connection]).toInstance(connection)
    bind(classOf[DB]).toInstance(DB(connection))
    bind(classOf[TokenMediaConnector]).asEagerSingleton()
    bindActor[SchedulerActor]("scheduler-actor")
    bind(classOf[Job]).asEagerSingleton()

  }

}
