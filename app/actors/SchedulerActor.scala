package actors

import javax.inject.Inject

import akka.actor.Actor
import connectors.TokenMediaConnector
import domains.{Notification, TokenMedia}
import org.apache.commons.dbcp.DelegatingConnection
import org.joda.time.DateTime
import org.postgresql.{PGConnection, PGNotification}
import play.api.http.Status._
import play.api.libs.json.Json
import play.api.libs.ws.WSResponse
import scalikejdbc._

import scala.concurrent.Future


class SchedulerActor @Inject()(tokenMediaConnector: TokenMediaConnector, db: DB) extends Actor {

  import context.dispatcher

  override def preStart() = {
    db.autoClose(false)
    db.localTx { implicit session =>
      sql"LISTEN events".execute().apply()
    }
  }

  override def postStop() = {
    db.close()
  }

  def receive = {
    case "tick" => {
      db.readOnly { implicit session =>
        val pgConnection = db.conn.asInstanceOf[DelegatingConnection].getInnermostDelegate.asInstanceOf[PGConnection]
        val notifications = Option(pgConnection.getNotifications).getOrElse(Array[PGNotification]())


        notifications.foreach { msg =>
          println(s"Received for: ${msg.getName} from process with PID: ${msg.getPID}")
          println(s"Received data: ${msg.getParameter} ")

          import domains.Notification.fmt

          val notification = Json.parse(msg.getParameter).as[Notification]
          val tokenMedia = notification.data

          notification.action match {
            case "INSERT" => handleNotification(tokenMedia, tokenMediaConnector.create, CREATED)
            case "UPDATE" if (tokenMedia.deleted) => handleNotification(tokenMedia, tokenMediaConnector.delete, OK)
            case _ =>
          }
        }
      }
    }
  }

  private def handleNotification(tokenMedia: TokenMedia, f: TokenMedia => Future[WSResponse], expectedStatus: Int) = {
    f(tokenMedia).map { response =>
      if (response.status == expectedStatus)
        updateRecord(tokenMedia)
    }
  }

  private def updateRecord(tokenMedia: TokenMedia) =
    db.localTx { implicit session =>
      sql"update token_x_media set updatedAt = ${DateTime.now} where identity_token = ${tokenMedia.identity_token} and media = ${tokenMedia.media}".update.apply()
    }
}