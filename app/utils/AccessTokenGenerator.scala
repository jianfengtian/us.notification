package utils

import org.joda.time.DateTime._
import pdi.jwt.{JwtAlgorithm, JwtClaim, JwtJson}

class AccessTokenGenerator(encodingSecret: String, lifetimeInSec: Int) {
  import AccessTokenGenerator.jwtAlgorithm

  def newAccessToken(userId: String, grants: Option[String]): AccessToken = {
    val claim = picplayJwtClaim(userId, grants)
    val token = JwtJson.encode(claim, encodingSecret, jwtAlgorithm)
    AccessToken(token, claim.expiration)
  }

  private def picplayJwtClaim(userId: String, grants: Option[String]): JwtClaim =
    JwtClaim(
      subject = Some(userId),
      expiration = Some(now().plusSeconds(lifetimeInSec).toDate.getTime),
      issuer = Some("picplay.com"),
      issuedAt = Some(now().toDate.getTime)
    ) + ("grants", grants.getOrElse(""))
}

object AccessTokenGenerator {
  val jwtAlgorithm = JwtAlgorithm.HS256
}

case class AccessToken(token: String, expiryTime: Option[Long])
