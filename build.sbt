name := """notification"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  cache,
  ws,
  "org.postgresql" % "postgresql" % "9.4-1200-jdbc41",
  "org.scalikejdbc" %% "scalikejdbc"       % "2.2.8",
  "com.pauldijou" %% "jwt-core" % "0.8.1",
  "com.pauldijou" %% "jwt-play-json" % "0.8.1",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
)

